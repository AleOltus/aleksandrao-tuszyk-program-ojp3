

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.Random;

import javax.swing.JPanel;

public class Game extends JPanel implements Runnable, MouseListener {

	
	private static final long serialVersionUID = 1L;
	private static final byte SZEROKOSC_PLANSZY = 20;
	private static final byte WYSOKOSC_PLANSZY = 20;
	private static final byte POLE_SZ =20, POLE_WY =20;
	private static final int SZEROKOSC = SZEROKOSC_PLANSZY*POLE_SZ;
	private static final int WYSOKOSC = (WYSOKOSC_PLANSZY*POLE_WY)+20;
	private Random generator = new Random();
	private boolean stan;
	
	private Image obraz;
	private Graphics2D grafika;
	private Thread watek;
	private int opoznienie =150; 
	private int ileRuch, perfectRuch, poziom;
	
	private boolean [][] plansza;
	
	
	public Game(){
		super();
		setPreferredSize(new Dimension(SZEROKOSC,WYSOKOSC));
		obraz = new BufferedImage(SZEROKOSC,WYSOKOSC, BufferedImage.TYPE_INT_RGB);
		grafika = (Graphics2D) obraz.getGraphics();
	}
	
	public void addNotify(){
		super.addNotify();
		watek = new Thread(this); addMouseListener(this);watek.start();
	}

	
	public void run() {
		
		ileRuch=0;perfectRuch=0;poziom =3 ; stan = true;
		plansza = new boolean[SZEROKOSC_PLANSZY][WYSOKOSC_PLANSZY];
		grafika.setFont(new Font("System", Font.BOLD,12)); 
		setPlansza();
		do{
			drukGrafika();
			drukNaEkran();
			
			try{watek.sleep(opoznienie);} catch (InterruptedException e) {e.printStackTrace();}
		}while(true);
		
		
		
	}
	private void drukNaEkran(){
		Graphics ekran = getGraphics();
		ekran.drawImage(obraz,0,0,null);
		ekran.dispose();
	}
	
	private void drukGrafika(){
		/*grafika.setColor(Color.WHITE);
		grafika.drawString("Tekst",10,10);*/
		grafika.setColor(Color.WHITE); grafika.fillRect(0, 0, SZEROKOSC, WYSOKOSC);drukPlansza();
		if(ileRuch>perfectRuch) {grafika.setColor(Color.RED);stan =false;} else grafika.setColor(Color.BLACK);
		grafika.setColor(Color.BLACK);grafika.drawString("Ruch" +ileRuch+" / "+perfectRuch, 10, WYSOKOSC_PLANSZY*POLE_WY+15);
		grafika.setColor(Color.BLACK);
		if(!stan) grafika.drawString("Game Over  Wynik:"+poziom , 100, WYSOKOSC_PLANSZY*POLE_WY+15);
	}
	
	private void drukPlansza(){
		int pola = 0;
		for(int x=0; x<SZEROKOSC_PLANSZY; x++)
			for(int y= 0; y <WYSOKOSC_PLANSZY; y++){
				if(plansza[x][y] ) {grafika.setColor(Color.GREEN);pola++;} else grafika.setColor(Color.WHITE);
				grafika.fillRect(x*POLE_SZ, y*POLE_WY, POLE_SZ, POLE_WY);
				grafika.setColor(Color.GRAY);grafika.drawRect(x*POLE_SZ, y*POLE_WY, POLE_SZ, POLE_WY);
				
			}
		if(pola==0 &&  stan ){poziom++; setPlansza();}
	
	}
	private void setPole(int x, int y){
		plansza[x][y] = !plansza[x][y];
		if(x-1>=0) plansza[x-1][y] = !plansza[x-1][y];
		if(x+1<=SZEROKOSC_PLANSZY-1) plansza[x+1][y] = !plansza[x+1][y];
		if(y-1>=0) plansza[x][y-1] = !plansza[x][y-1];
		if(y+1<=WYSOKOSC_PLANSZY-1) plansza[x][y+1] = !plansza[x][y+1];
	}
	
	private void setPlansza(){
		int min = 0, max = 0;
		perfectRuch=0;ileRuch=0;		
		int x,y;
		
		if(poziom <7 ) {max=5; min=8;}else 
		if (poziom <20) {max=10; min=5;} else
		if (poziom <30) {max=15; min=2;} else {max =SZEROKOSC_PLANSZY; min = 0;}
		
		for(int i = 0; i <poziom ; i++){
			x=generator.nextInt(max); x+=min;
			y=generator.nextInt(max); y+=min;
			setPole(x,y);perfectRuch++;
		}
	}

	
	public void mouseClicked(MouseEvent arg0) {}
	public void mouseEntered(MouseEvent arg0) {}
	public void mouseExited(MouseEvent arg0) {}
	public void mousePressed(MouseEvent arg0) {
		if(arg0.getY()<=WYSOKOSC_PLANSZY*POLE_WY){
		int x = arg0.getX()/POLE_SZ;
		int y =arg0.getY()/POLE_WY;
		ileRuch++;setPole(x,y);
		}
	}
	
	public void mouseReleased(MouseEvent arg0) {}
}
