Aleksandra Ołtuszyk

**1. Opis Projektu**
    Prosta gra logiczna stworzona w języku java.  Polega na wykorzystaniu jak najmniejszej liczby ruchów aby z przestawionej planszy znikneły zamalowane kwadraty. 
    
  **2.  Uruchomienie**
   język - JAVA
   środowisko - eclipse
   
   Po uruchomieniu programu wyskakuje nam  plansza gotowa do gry w konsoli programu.
   
   
  **3. Cel**
   Gra, w której zasady opracowano w taki sposób, że jej elementy służą poprawie umiejętności lub poszerzeniu zasobów wiedzy graczy. Poza walorami czysto rozrywkowymi, gra taka stymuluje rozwój graczy w określonych kierunkach i obszarach. , ceni się ją za skuteczność, osiąganą dzięki zwiększeniu atrakcyjności procesu dydaktycznego. Mimo że jej podstawowym celem jest edukacja, stanowiąca formę rozrywki, co niesie dodatkową wartość poznawczą – aktywizacja układu limbicznego, związanego między innymi wywoływaniem pozytywnych emocji (odczuwaniem przyjemności; zob. hedonizm), pobudza mózg do pracy i rozwoju. Jest to naturalny dla człowieka mechanizm przetrwania i przystosowania: przyjemność uczy nas odróżniać to, co dla nas dobre i korzystne (stymuluje procesy uczenia się)

Niemal każda gra zawiera elementy edukacyjne, jednak w grach edukacyjnych nauka i trening należą do głównych celów, a nie są tylko wartościami dodanymi. Gra tym się rózni od innych, że została opracowana przede wszystkim w celu nauczania i stymulowania rozwoju, a rozrywka jest tu formą przekazu wiedzy i narzędziem do poprawy umiejętności.

**1. Project Description**
    A simple puzzle game developed in Java. It is based on the minimum number of moves to the board been moved the painted squares were gone.
    
 ** 2. Run**
   language - JAVA
   Environment - eclipse
   
   After starting the program pops us board ready to play.
   
   
** 3. Purpose**
  A game in which the rules are designed in such a way that its elements serve to improve skills or broadening the knowledge base of players. In addition to the qualities of pure entertainment, this game stimulates the development of players in certain directions and areas. , Valued it for its efficiency, achieved by increasing the attractiveness of the teaching process. Although its primary purpose is education, which is a form of entertainment, which provides the added educational value - activation of the limbic system, associated, among others, provoking positive emotions (feeling of pleasure; see. Hedonism) stimulates the brain to work and development. This is a natural mechanism for human survival and adaptation: the pleasure teaches us to distinguish between what is good for us and beneficial (to stimulate learning processes)

Almost every game contains educational elements, but educational games learning and training are the main objectives, and not just added values. The game differs from others that has been developed primarily for the purpose of teaching and stimulate the development and the entertainment is there a form of transfer of knowledge and tools to improve skills.

